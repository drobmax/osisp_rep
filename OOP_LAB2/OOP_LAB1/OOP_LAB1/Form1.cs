﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace OOP_LAB1
{
    public partial class Form1 : Form
    {
        Graphics graph;
        public static Point startPoint, endPoint;
        private List<Object> figureList = new List<object>();
        private List<Type> typeList = new List<Type>();
        int index = 0;
        public static int canDraw = 0;
        public Form1()
        {
            InitializeComponent();
            graph = CreateGraphics();
            graph.Clear(Color.White); 
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            string[] libs = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.dll", SearchOption.AllDirectories);
            foreach (var lib in libs)
            {
                Assembly asm = Assembly.LoadFile(lib);
                Type[] fig = asm.GetTypes();
                foreach(var temp in fig)
                    if ((fig != null) && (temp.IsSubclassOf(typeof(Shape))))
                {
                    typeList.Add(temp);
                    comboBox1.Items.Add(temp.Name);
                }
            }
        }


        public void Form1_Paint(object sender, PaintEventArgs e)
        {
        }


        private void button6_Click(object sender, EventArgs e)
        {
            graph.Clear(Color.White); 
        }


        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            

            switch (canDraw)
            {
                case 0:
                    canDraw = 1;
                    startPoint = e.Location;
                    break;
                case 1:
                    endPoint = e.Location;
                    canDraw = 2;
                    break;
            }
            if (comboBox1.Text == "") 
            {
                canDraw = 0;
                MessageBox.Show("Чёйто не то!");
            }
            else
            try
            {
                
                if (canDraw == 2)
                {
                    canDraw = 0;
                    var shape = (Shape)Activator.CreateInstance(typeList[index], endPoint, startPoint);
                    shape.Draw(graph);
                }
            }
            catch
            {
                canDraw = 0;
                MessageBox.Show("Чёйто не то!");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            index = comboBox1.Items.IndexOf(comboBox1.Text);
            canDraw = 0;
        }
    }
}
