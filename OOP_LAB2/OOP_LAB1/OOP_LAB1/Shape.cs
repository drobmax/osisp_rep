﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace OOP_LAB1
{
    public abstract class Shape
    {
        protected float startX, startY, endX, endY;
        protected Pen p;
        public Shape() { }
        public abstract void Draw(Graphics gr);
    }
}
