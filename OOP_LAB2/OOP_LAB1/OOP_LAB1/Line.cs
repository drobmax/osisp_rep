﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace OOP_LAB1
{
    class Line : Shape
    {
        public Line(){}

        public Line(Point par1, Point par2)
        {
            startX = par1.X;
            startY = par1.Y;
            endX   = par2.X;
            endY =   par2.Y;
            p = new Pen(Color.Aqua, 3);
        }

        public override void Draw(Graphics e)
        {
            e.DrawLine(p, startX, startY, endX, endY);
        }
    }
}
