﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TriangleLib
{
    class Triangle : OOP_LAB1.Shape
    {
        Point[] p1 = new Point[3];
        public Triangle() { }
        public Triangle(Point par1, Point par2)
        {
            startX = par1.X;
            startY = par1.Y;
            endX = par2.X;
            endY = par2.Y;

            p = new Pen(Color.Red, 3);

            p1[0] = new Point((int)startX, (int)startY);
            p1[1] = new Point((int)endX, (int)endY);
            p1[2] = new Point((int)startX - (int)(endX - startX), (int)endY);
        }
        public override void Draw(Graphics e)
        {
            e.DrawPolygon(p, p1);
        }
    }
}
