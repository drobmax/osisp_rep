﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PolyLine
{
    class PolyLine : OOP_LAB1.Shape
    {
        public PolyLine() { }

        public PolyLine(Point par1, Point par2)
        {
            startX = par1.X;
            startY = par1.Y;
            endX = par2.X;
            endY = par2.Y;
            p = new Pen(Color.Aqua, 3);
        }

        public override void Draw(Graphics e)
        {
            e.DrawLine(p, startX, startY, endX, endY);
            OOP_LAB1.Form1.canDraw = 1;
            OOP_LAB1.Form1.startPoint = OOP_LAB1.Form1.endPoint;
        }
    }
}

