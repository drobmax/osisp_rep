﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace EllipseLibrary
{
    class Ellipse : OOP_LAB1.Shape
    {
        public Ellipse() { }

        public Ellipse(Point par1, Point par2)
        {
            startX = par1.X;
            startY = par1.Y;
            endX = par2.X;
            endY = par2.Y;
            p = new Pen(Color.Yellow, 3);
        }

        public override void Draw(Graphics e)
        {
            e.DrawEllipse(p, startX, startY,(endX - startX),(endY - startY));
        }
    }
}
