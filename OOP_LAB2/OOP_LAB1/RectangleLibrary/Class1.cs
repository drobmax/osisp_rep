﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace RectangleLibrary
{
    class RectangleLib : OOP_LAB1.Shape
    {
        public RectangleLib() { }

        public RectangleLib(Point par1, Point par2)
        {
            startX = par1.X;
            startY = par1.Y;
            endX = par2.X;
            endY = par2.Y;
            p = new Pen(Color.Green, 3);
        }
        
        public override void Draw(Graphics e)
        {
            int tempX=0,tempY=0;
            if (startX > endX)
                {
                        tempX = (int)startX;
                        startX = endX;
                        endX = tempX;
                }
            if(startY > endY)
                    {
                        tempY = (int)startY;
                        startY = endY;
                        endY = tempY;
                    }
            e.DrawRectangle(p, startX, startY, Math.Abs(endX - startX), Math.Abs(endY - startY));
        }
    }
}
