﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Circle
{
    public class Round : Figure.Fig
    {
        float x, y, w, h;
        public Round(float par1, float par2, float par3, float par4)
        {
            x = par1;
            y = par2;
            w = par4;
            h = par3;
        }
        public override void Draw(Graphics e)
        {
            Pen p = new Pen(Color.SteelBlue, 4);
            e.DrawEllipse(p,x,y,w,h);
        }
    }
}
