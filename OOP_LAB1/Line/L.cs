﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Line
{
    public class CLine: Figure.Fig
    {
        float x, y, x1, y1;
        public CLine (float par1, float par2, float par3, float par4)
        {
            x = par1;
            y = par2;
            x1 = par3;
            y1 = par4;
        }
        public override void Draw(Graphics e)
        {
            e.DrawLine(Pens.Blue,x, y, x1, y1);   
        } 
    }
}
