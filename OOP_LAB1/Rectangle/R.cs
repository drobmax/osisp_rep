﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Rectangle
{
    public class Rect: Figure.Fig
    {
        public float dx, dy , w , h;
        public Rect(float par1, float par2, float par3, float par4)
        {
            dx = par1;
            dy = par2;
            w = par4;
            h = par3;
        }
        public override void Draw(Graphics e)
        {
            e.DrawRectangle(Pens.Green,dx,dy,w,h);
        }
    }
}
