﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OOP_LAB1
{
    class Rectangle:Figure
    {
        float dx, dy , w , h;
        Graphics Pen;
        public Rectangle(Graphics e, float par1, float par2, float par3, float par4)
        {
            Pen = e;
            dx = par1;
            dy = par2;
            w = par4;
            h = par3;
        }
        public override void Draw()
        {
            Pen.DrawRectangle(Pens.Green,dx,dy,w,h);
        }
    } 
}
