﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Line;
using Rectangle;
using Triangle1;
using Circle;
namespace OOP_LAB1
{
    public partial class Form1 : Form
    {
        Graphics graph;
        List<Figure.Fig> shape = new List<Figure.Fig>();
        int t = 0;
        int t1 = 0;
        int t2 = 0;
        int t3 = 0;
        public Form1()
        {
            InitializeComponent();
            graph = CreateGraphics();
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            shape.Add(new Rect(150, 10, 100, 50));
            shape.Add(new CLine(10, 10, 100, 100));
            shape.Add(new Tri(250, 10, 250, 100, 330, 100));
            shape.Add(new Round(450,10,100,100));
        }
        public void Form1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Rect r = new Rect((float)numericUpDown1.Value, (float)numericUpDown2.Value, (float)numericUpDown3.Value, (float)numericUpDown3.Value);
            t2 += 10;
            r.Draw(graph);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach (var temp in shape)
            {
                temp.Draw(graph);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CLine l = new CLine((float)numericUpDown1.Value, (float)numericUpDown2.Value, (float)numericUpDown1.Value + (float)numericUpDown3.Value, (float)numericUpDown2.Value + (float)numericUpDown3.Value);
            l.Draw(graph);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Round l = new Round((float)numericUpDown1.Value, (float)numericUpDown2.Value, (float)numericUpDown3.Value, (float)numericUpDown3.Value);
            l.Draw(graph);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Tri l = new Tri((int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown1.Value + (int)numericUpDown3.Value, (int)numericUpDown2.Value, (int)numericUpDown1.Value, (int)numericUpDown2.Value + (int)numericUpDown3.Value);
            t3 += 10;
            l.Draw(graph);

        }
    }
}
