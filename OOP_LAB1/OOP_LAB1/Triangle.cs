﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OOP_LAB1
{
    class Triangle:Figure
    {
        float[] x = { 100, 50, 200 };  // Массив х-кординат треугольника
        float[] y = { 100, 200, 300 };
        float dx,dy;
        Graphics Pen;
        public Triangle(Graphics e,float par1,float par2)
        {
            Pen = e;
            dx = par1;
            dy = par2;
        }
        public override void Draw()
        {
            Pen.DrawLine(Pens.Red, x[0] + dx, y[0] + dy, x[1] + dx, y[1] + dy);
            Pen.DrawLine(Pens.Red, x[1] + dx, y[1] + dy, x[2] + dx, y[2] + dy);
            Pen.DrawLine(Pens.Red, x[2] + dx, y[2] + dy, x[0] + dx, y[0] + dy);
        } 
    }
}
