﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Triangle
{
    public class Tri: Figure.Fig
    {
        Point [] p1 = new Point[3];
        public Tri(int par1,int par2, int par3, int par4, int par5,int par6)
        {
            p1[0] = new Point(par1, par2);
            p1[1]= new Point(par3, par4);
            p1[2] = new Point(par5, par6);                  
        }
        public override void Draw(Graphics e)
        {
            e.DrawPolygon(Pens.Green, p1);      
        } 
    }
}
