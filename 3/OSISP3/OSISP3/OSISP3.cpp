// OSISP3.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <conio.h>
#include <iostream>
int _tmain(int argc, _TCHAR* argv[]){
	HANDLE hMutex = CreateMutex(NULL, FALSE, L"Global\\ProcessMutex");
	if (GetLastError() == ERROR_ALREADY_EXISTS)
		std::cout << "Error, mutex created" << std::endl;
	CHAR startBuffer[255] = "BLAHBLAH";
	CHAR buffer[255];
	HANDLE hMemorySpace = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, 255, L"SharedMemory");
	void* adr =  MapViewOfFile(hMemorySpace, FILE_WRITE_ACCESS, 0, 0, 255);
	if (GetLastError() == ERROR_ALREADY_EXISTS)
		hMemorySpace = OpenFileMapping(FILE_WRITE_ACCESS, FALSE, L"SharedMemory");
	else{
		std::cout<< startBuffer<<std::endl;
		CopyMemory(adr, startBuffer, 255);
	}
	getchar();
	while(true){
		std::cout << "Wait..." << std::endl;
		WaitForSingleObject(hMutex, INFINITE);
		std::cout << "Enter critical section" << std::endl;
		memcpy(buffer, adr, 255);
		std::cout << buffer << std::endl;
		Sleep(5000);
		ReleaseMutex(hMutex);
		std::cout <<"Leave critical section" << std::endl;
	}
	std::cout << "the end" << std::endl;
	UnmapViewOfFile(adr);
	CloseHandle(hMemorySpace);
	CloseHandle(hMutex);
	system("PAUSE");
	return 0;
}