﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoolTables
{
    [Serializable]
    public class TablesForPool: Gym.ForGym
    {
        public string KindOfPool { get; set; }
        public TablesForPool(string kindOfPool,string level,string name)
        {
            Name = name;
            Level = level;
            KindOfPool = kindOfPool;
        }
        public TablesForPool() { }
    }
}
