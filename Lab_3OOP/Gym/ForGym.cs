﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace Gym
{
    [Serializable]
    public class ForGym : Furniture.AllFurniture
    {
        public string Level { get; set; }

        public ForGym(string level,string name)
        {
            Name = name;
            Level = level;
        }
        public ForGym() { }
        
    }
}
