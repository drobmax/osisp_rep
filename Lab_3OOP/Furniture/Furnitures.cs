﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Furniture
{
    [Serializable]
    public class AllFurniture
    {
        public string Name { get; set; }

        public AllFurniture(string name)
        {
            Name = name;
        }
        public AllFurniture() { }
    }
}