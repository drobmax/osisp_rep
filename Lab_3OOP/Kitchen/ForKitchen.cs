﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kitchen
{
    [Serializable]
    public class ForKitchen : Furniture.AllFurniture
    {
        public string WetResistance { get; set; }
        public ForKitchen(string wetResistance,string name) 
        {
            Name = name;
            WetResistance = wetResistance;
        }
        public ForKitchen() { }
    }
}
