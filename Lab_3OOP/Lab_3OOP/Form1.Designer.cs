﻿namespace Lab_3OOP
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainClassGrid = new System.Windows.Forms.DataGridView();
            this.subClassGrid = new System.Windows.Forms.DataGridView();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mainClassGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subClassGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // mainClassGrid
            // 
            this.mainClassGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mainClassGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mainClassGrid.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mainClassGrid.Location = new System.Drawing.Point(12, 68);
            this.mainClassGrid.Name = "mainClassGrid";
            this.mainClassGrid.Size = new System.Drawing.Size(172, 302);
            this.mainClassGrid.TabIndex = 0;
            this.mainClassGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.mainClassGrid_CellContentClick);
            this.mainClassGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.mainClassGrid_CellMouseDoubleClick);
            // 
            // subClassGrid
            // 
            this.subClassGrid.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.subClassGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.subClassGrid.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.subClassGrid.Location = new System.Drawing.Point(190, 68);
            this.subClassGrid.Name = "subClassGrid";
            this.subClassGrid.Size = new System.Drawing.Size(183, 302);
            this.subClassGrid.TabIndex = 2;
            this.subClassGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.subClassGrid_CellContentClick);
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView3.Location = new System.Drawing.Point(379, 68);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(174, 302);
            this.dataGridView3.TabIndex = 3;
            this.dataGridView3.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellContentClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "GO";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 406);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.subClassGrid);
            this.Controls.Add(this.mainClassGrid);
            this.Name = "Form1";
            this.Text = "Furniture for house";
            ((System.ComponentModel.ISupportInitialize)(this.mainClassGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subClassGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView mainClassGrid;
        private System.Windows.Forms.DataGridView subClassGrid;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button1;
    }
}

