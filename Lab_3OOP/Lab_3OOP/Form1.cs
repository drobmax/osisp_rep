﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Lab_3OOP
{
    public partial class Form1 : Form
    {
        private List<Type> typeList = new List<Type>();
        private List<Object> objectList = new List<Object>();
        private List<TreeNode> nodeList = new List<TreeNode>();
        int _selectedIndex=5;
        private Type BaseType = typeof(Furniture.AllFurniture);

        public Form1()
        {
            InitializeComponent();
            TreeNode addedNode;
            dataGridView1.RowHeadersWidth = 5;
            string[] libs = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.dll", SearchOption.AllDirectories);
            addedNode = treeView1.Nodes.Add(BaseType.Name);
            BuildTree(BaseType,libs, addedNode);
        }

        private void BuildTree(Type BaseType, string[] libs, TreeNode node)
        {
            foreach (var lib in libs)
            {
                Assembly asm = Assembly.LoadFile(lib);
                Type[] fig = asm.GetTypes();
                foreach (var type in fig)
                {
                    if ((BaseType != null) && (type.BaseType == BaseType))
                    {
                        typeList.Add(type);
                        comboBox1.Items.Add(type.Name);
                        node.Nodes.Add(type.Name);
                        nodeList.Add(node.LastNode);
                        BuildTree(type, libs, node.LastNode);
                    }
                }
            }
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] param= new string[dataGridView1.RowCount-1];
            for (int i = 0; i < dataGridView1.RowCount-1 ; i++)
            {
                param[i] = dataGridView1.Rows[i].Cells[1].Value.ToString();
            }
            var obj = (Furniture.AllFurniture)Activator.CreateInstance(typeList[_selectedIndex],param);
            objectList.Add(obj);
            ShowObjectInTree(obj);
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedIndex = comboBox1.SelectedIndex;
            var objectInfo = typeList[_selectedIndex].GetProperties();
            int i = 0;
            
            foreach (var obj in objectInfo)
            {
                dataGridView1.RowCount = i+1;
                dataGridView1.Rows.Add(obj.Name);
                i++;
            }
        }

        private void xMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var serializer = new XmlSerializer(typeof(List<object>),typeList.ToArray());
            using (Stream fStream = new FileStream("ListXML.xml", FileMode.Create, FileAccess.Write, FileShare.None))
                serializer.Serialize(fStream, objectList);
            MessageBox.Show("Completed!");
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void ShowObjectInTree(Furniture.AllFurniture obj)
        {
            foreach (var list in nodeList)
            {
                if (list.Text.Equals(obj.GetType().Name))
                {
                    list.Nodes.Add(obj.Name);
                    treeView1.Refresh();
                    return;
                }
            }
        }

        private void bINToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var serializer = new BinaryFormatter();
            using (Stream fStream = new FileStream("ListBinary.dat", FileMode.Create, FileAccess.Write, FileShare.None))
                serializer.Serialize(fStream, objectList);
            MessageBox.Show("Completed!");
        }

        private void bINToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var serializer = new BinaryFormatter();
            List<object> objectList1;
            using (Stream fStream = new FileStream("ListBinary.dat", FileMode.Open, FileAccess.Read, FileShare.None))
                objectList1 = (List<object>)serializer.Deserialize(fStream);
            foreach (var obj in objectList1)
            {
                objectList.Add(obj);
                ShowObjectInTree((Furniture.AllFurniture)obj);
            }
            MessageBox.Show("Completed!");
        }

        private void xMLToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
            List<object> objectList1;
            var serializer = new XmlSerializer(typeof(List<object>), typeList.ToArray());
            using (Stream fStream = new FileStream("ListXML.xml", FileMode.Open, FileAccess.Read, FileShare.None))
                objectList1 = (List<object>)serializer.Deserialize(fStream);
            foreach (var obj in objectList1)
            {
                objectList.Add(obj);
                ShowObjectInTree((Furniture.AllFurniture)obj);
            }
            MessageBox.Show("Completed!");
        }

        private void tXTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TXTSerializer serializer = new TXTSerializer(typeList.ToArray());
            serializer.Serialize(objectList, @"ListTXT.txt");
            MessageBox.Show("Completed!");
        }

        private void tXTToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            TXTSerializer serializer = new TXTSerializer(typeList.ToArray());
            List<object> tempListObject = serializer.Deserialize(@"ListTXT.txt");
            foreach (var obj in tempListObject)
            {
                objectList.Add(obj);
                ShowObjectInTree((Furniture.AllFurniture)obj);
            }
            MessageBox.Show("Completed!");
        }

        private void выйтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
