﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PingPongTables
{
    [Serializable]
    public class TablesForPingPong:Gym.ForGym
    {
        public string Construction { get; set; }
        public TablesForPingPong(string construction, string level, string name) 
        {
            Name = name;
            Level = level;
            Construction = construction;
        }
        public TablesForPingPong() { }
    }
}
