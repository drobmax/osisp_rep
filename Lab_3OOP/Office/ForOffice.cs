﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Office
{
    [Serializable]
    public class ForOffice : Furniture.AllFurniture
    {
        public string Warranty { get; set; }
        public ForOffice(string warranty,string name) 
        {
            Name = name;
            Warranty = warranty;
        }
        public ForOffice() { }
    }
}
