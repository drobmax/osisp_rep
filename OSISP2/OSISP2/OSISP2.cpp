// OSISP2.cpp: ���������� ����� ����� ��� ����������� ����������.
//
//libraries
#include "stdafx.h"
#include "stdio.h"
#include "conio.h"
#include "windows.h"
#include "stdlib.h"
#include "time.h"
#include <iostream>
#include <fstream>
#include <functional>
#include <thread>
#include <memory>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <queue>
//stuff from h-files
#ifdef THREADPOOL_EXPORTS
#define THREADPOOL_API __declspec(dllexport)
#else
#define THREADPOOL_API __declspec(dllimport)
#endif
#ifdef WORKERTHREAD_EXPORTS
#define WORKERTHREAD_API __declspec(dllexport)
#else
#define WORKERTHREAD_API __declspec(dllimport)
#endif	

void 			TaskFunction(int waitTime);
void 			WriteMessage(char str[], int threadId);
CRITICAL_SECTION	fileMutex;
CRITICAL_SECTION	addTaskMutex;

using namespace std;

class WorkerThread{
	public:
		WORKERTHREAD_API				WorkerThread( void (&LogFunc)(char str[255],  int threadId));
		WORKERTHREAD_API				~WorkerThread();
		WORKERTHREAD_API void			WakeThread();
		function<void(int time)>		task;
		int								waitTime;
		bool							notEmpty;
		bool							canDestroy;
		DWORD							id;
		HANDLE							justThread;
		CRITICAL_SECTION				justMutex;
		CONDITION_VARIABLE				access;
		bool							enabled;
		void(*Log)(char str[255],  int threadId);		
};

	DWORD WINAPI ThreadFunction(LPVOID lParam);

	WorkerThread::WorkerThread( void (&LogFunc)(char str[255], int threadId)){
		Log = LogFunc;
		canDestroy = false;
		notEmpty = false;
		waitTime = 0;
		enabled = true;
		InitializeConditionVariable(&access);
		InitializeCriticalSection(&justMutex);
		justThread = CreateThread(NULL, 0, ThreadFunction, (void*) this, 0, &id);
	}

	WorkerThread::~WorkerThread(){
		enabled = false;
		WakeConditionVariable(&access);
		WaitForSingleObject(justThread, INFINITE);
		CloseHandle(justThread);
	}

	void WorkerThread::WakeThread(){
		WakeConditionVariable(&access);
	}

	DWORD WINAPI ThreadFunction(LPVOID lParam){
		WorkerThread *pThread = (WorkerThread*)lParam;
		char str[255];
		strcpy_s(str, "Thread created");
		pThread->Log(str,  pThread->id);
		while (pThread->enabled){
			EnterCriticalSection(&pThread->justMutex);
			SleepConditionVariableCS(&pThread->access, &pThread->justMutex, INFINITE);
			if ((pThread->notEmpty) && (!pThread->canDestroy)){
				str[0] = '\0';
				strcpy_s(str, "Task added");
				pThread->Log(str,  pThread->id);
				try{
					pThread->task(pThread->waitTime);
				}
				catch (...){
					pThread->canDestroy = true;
					strcpy_s(str, "Error: Something wrong");
					pThread->Log(str,  pThread->id);
					//strcpy_s(str, "Thread destroyed");
					//pThread->Log(str,  pThread->id);
					LeaveCriticalSection(&pThread->justMutex);
					continue;
				}
				str[0] = '\0';
				strcpy_s(str, "Task done");
				pThread->Log(str,  pThread->id);
				pThread->waitTime = 0;
				pThread->notEmpty = false;
			}
			LeaveCriticalSection(&pThread->justMutex);
		}
		strcpy_s(str, "Thread destroyed");
		pThread->Log(str,  pThread->id);
		ExitThread(0);
	}

class ThreadPool{
	public:
		THREADPOOL_API			ThreadPool(int n, void(&LogFunc)(char str[255], int threadId));
		THREADPOOL_API			~ThreadPool();
		THREADPOOL_API int		SetTask(int time, void (&task)(int time));
	private:
		THREADPOOL_API int		GetFreeThread();
		vector<shared_ptr<WorkerThread>>	workerList;
		void(*Log)(char str[255],  int threadId);
};


	ThreadPool::ThreadPool(int n, void(&LogFunc)(char str[255], int threadId)){
		Log = LogFunc;
		for (int i = 0; i < n; i++){
			time(NULL);
			shared_ptr<WorkerThread> pWorker(new WorkerThread( *Log));
			workerList.push_back(pWorker);
		}
	}
	ThreadPool::~ThreadPool(){}

	int ThreadPool::GetFreeThread(){
		int numb = 0;
		if (workerList.size() == 0){
			return -2;
		}
		while (numb != workerList.size()){
			if (workerList[numb]->canDestroy){
				workerList.erase(workerList.begin() + numb);
			}
			else{
				numb++;
			}
		}
		if (workerList.size() == 0){
			return -2;
		}
		for (int j = 0; j < workerList.size(); j++){
			if (!(workerList[j]->notEmpty))
			return j;
		}
		return -1;
	}

	int ThreadPool::SetTask(int time, void (&task)(int time)){
		int numb = 0;
		if ((numb = this->GetFreeThread()) == -2){
			return -1;
		}
		if ((numb = this->GetFreeThread()) == -1){
			char str[255];
			strcpy_s(str, "Warning : There are not any free threads");
			Log(str, GetThreadId(NULL));
			cout << "Warning : There are not any free threads\n\n";
			return 0;
		}
		else{
			workerList[numb]->task = task;
			workerList[numb]->waitTime = time;
			workerList[numb]->notEmpty = true;
			workerList[numb]->WakeThread();
		}
		return 0;
	}

int _tmain(int argc, _TCHAR* argv[]){
	int n = 0;
	int t = 0;
	printf("Enter number of threads\n");
	printf("n = ");
	scanf_s("%d", &n);
	printf("Enter user's fuction runtime\n");
	scanf_s("%d",&t);
	t*=1000;
	InitializeCriticalSection(&fileMutex);
	shared_ptr<ThreadPool> pThreadPool(new ThreadPool(n, WriteMessage));
	srand(time(0));
	InitializeCriticalSection(&addTaskMutex);
	while (true){
		printf("<enter>- add task \n<q>-\t exit.\n");
		char c;
		fflush(stdin);
		scanf_s("%c", &c);
		if (c == '\n'){
			EnterCriticalSection(&addTaskMutex);
			if (pThreadPool->SetTask(t, TaskFunction) == -1){
				pThreadPool->~ThreadPool();
				break;
			}
			LeaveCriticalSection(&addTaskMutex);
		}
		if (c == 'q'){
			pThreadPool->~ThreadPool();
			break;
		}
	}
	DeleteCriticalSection(&addTaskMutex);
	DeleteCriticalSection(&fileMutex);
	pThreadPool->~ThreadPool();
	printf("Pool successfully stoped\n");
	getchar();getchar();
	return 0;
}
	
void TaskFunction(int waitTime){
	Sleep(waitTime);
	srand(time(NULL));
	int a = rand()%1000;
	if (a%3==0)
		throw 1;
}
	
void WriteMessage(char str[255],  int threadId){
	EnterCriticalSection(&fileMutex);{
		ofstream f("myLogFile.log", ios::app);
		f << "ID:"<<threadId <<  " :" << str << "\n\n";
		f.close();
		cout <<"ID:"<<threadId <<  " :" << str << "\n\n";
	}
	LeaveCriticalSection(&fileMutex);
}
